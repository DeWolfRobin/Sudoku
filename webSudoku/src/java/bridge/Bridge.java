/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bridge;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.json.*;
import sudokujavacode.*;
import java.util.Arrays;
/**
 *
 * @author Jonas
 */
@WebServlet(name = "Bridge", urlPatterns = {"/API/*"})
public class Bridge extends HttpServlet {

    private static final String PREFIX = "/webSudoku/API/";
    
    private JsonObject genSudoku(String widht, String height){
        if(widht == null || height == null){
            return Json.createObjectBuilder()
                    .add("error", "Missing data")
                    .add("widht",""+widht)
                    .add("height",""+height)
                    .build();
        } else {
            int left = Integer.parseInt(widht);
            int right = Integer.parseInt(height);
            return create(left, right);
        }     
    }
    
    private Field sudoku = new Field();
    
    private String convertToString(int[][] ar){
        String output = "";
        for (int row = 0; row < ar.length; row++) {
            output += Arrays.toString(ar[row]);
        }
        return output;
    }
    
    private JsonObject create(int left, int right) {
        sudoku.createField(left, right);
        sudoku.fillField(sudoku);
        sudoku.printArray(sudoku);
        return Json.createObjectBuilder()
                    .add("success", "No fail")
                    .add("Sudoku: ",""+convertToString(sudoku.getFieldArray()))
                    .build();
    }
        
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/plain;charset=UTF-8");
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            //String operation = request.getRequestURI().substring(PREFIX.length());
            String width = request.getParameter("width");
            String height = request.getParameter("height");
            
            JsonObject json = genSudoku(width, height);
            out.println(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
