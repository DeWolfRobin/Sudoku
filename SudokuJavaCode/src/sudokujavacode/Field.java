/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package sudokujavacode;

import java.util.Arrays;
import java.util.Random;
//shuffle array
import java.util.concurrent.ThreadLocalRandom; //shuffleArray(solutionArray);

/**
 *
 * @author Robin De Wolf, Jonas Huylebroecke
 */
public class Field {
    //-------------------------------------------------------------------------------
    //vars
    private int rowCount;
    private int columnCount;
    private int[][] fieldArray;
    //default array => scramble to fill sudoku
    //-------------------------------------------------------------------------------
    //editing
    
  static void shuffleArray(int[] ar){
    // If running on Java 6 or older, use `new Random()` on RHS here
    Random rnd = new Random();
    for (int i = ar.length - 1; i > 0; i--){
      int index = rnd.nextInt(i + 1);
      // Simple swap
      int a = ar[index];
      ar[index] = ar[i];
      ar[i] = a;
    }
  }
  public void fillField(Field val){
      
      
      for (int j = 0; j < val.getRowCount(); j++) {
        int[] scramble = new int[]{1,2,3,4,5,6,7,8,9};//numbers can differ
        shuffleArray(scramble);
          val.getFieldArray()[j] = scramble;
      }
      
      for (int i = 0; i < val.getRowCount(); i++) {
          if (arrayContainsAmount(i, getRowToArray(i, val))>=1) {
              System.out.println(i);
          }
      }

    }
    
    private int arrayContainsAmount(int number, int[] array) {
        int amount = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                amount++;
            }
        }
        return amount;
    }
  
  private int[] getRowToArray(int row, Field val) {
      int[] rowarray = new int[val.getColumnCount()];
      int [][] sudoku = val.getFieldArray();
      for (int columns = 0; columns < val.getColumnCount(); columns++) {
          rowarray[columns] = sudoku[row][columns];
      }
      return rowarray;
  }
  
   private int[] getColumnToArray(int Column, Field val) {
      int[] Columnarray = new int[val.getRowCount()];
      int [][] sudoku = val.getFieldArray();
      for (int rows = 0; rows < val.getColumnCount(); rows++) {
          Columnarray[rows] = sudoku[rows][Column];
      }
      return Columnarray;
  }
    
  
  
  // Implementing Fisher–Yates shuffle

    
    public void createField(int rows, int columns) {//werkt
        rowCount = rows;
        columnCount = columns;
        
        fieldArray = new int[getRowCount()][getColumnCount()];
        
        for (int i = 0; i < getRowCount(); i++) {
            
            for (int j = 0; j < getColumnCount(); j++) {
                fieldArray[i] = new int[getColumnCount()];
            }
        }
        
    }
    
    public void insert(int row, int column, int val, Field field){
        field.getFieldArray()[row][column] = val;
    }
    //-------------------------------------------------------------------------------
    //output
    public void printArray(Field field){
        
        for (int i = 0; i < field.getRowCount(); i++) {
            for (int j = 0; j < field.getColumnCount(); j++) {
            }
            System.out.println(Arrays.toString(field.getFieldArray()[i]));
            
        }
        
    }
    
     public void printArray(int[] field){
        
        for (int i = 0; i < field.length; i++) {
            System.out.println(field[i]);
            
        }
        
    }
    
    public int[][] getFieldArray() {
        return fieldArray;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }
    
}
