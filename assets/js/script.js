function showCustom(){
  $('#submit').css("margin-top", "0px")
  $('.custom').css("visibility", "visible")
}

function collapseCustom() {
  $('#submit').css("margin-top", "-85px")
  $('.custom').css("visibility", "hidden")
}

$(document).ready(function () {
    $('#custom').on('click', showCustom);
    $('.dif').on('click', collapseCustom);
});
